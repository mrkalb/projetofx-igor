/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DTO;

import java.io.Serializable;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;


/**
 *
 * @author igor
 */

public class Produto implements AbstractDto<Integer>,Serializable {
    

    private Integer id; 
    

    private StringProperty descricao; 
    

    private DoubleProperty precoUnitario;
    
    private DoubleProperty quantidade;  
    
    public Produto(String desc, Double preco, Double quant){
        this.descricao = new SimpleStringProperty(desc);
        this.precoUnitario = new SimpleDoubleProperty(preco);
        this.quantidade = new SimpleDoubleProperty(quant);
    }
    
    
    /*@ElementCollection
    @ManyToMany(mappedBy = "venda")
    private Venda venda; 
*/
    /**
     * @param id the id to set
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return the descricao
     */

    public String getDescricao(){
        return this.descricao.get();
    }
    
    public void setDescricao(String descricao){
        this.descricao.set(descricao);
    }
    

    public  double getPreçoUnitario(){
        return this.precoUnitario.get();
    }
    
    public void setPreco(Double preco){
        this.precoUnitario.set(preco);
        
    }
    

    public StringProperty getDescricaoProp() {
        return descricao;
    }

    /**
     * @param descricao the descricao to set
     */
    public void setDescricao(StringProperty descricao) {
        this.descricao = descricao;
    }

    /**
     * @return the preçoUnitario
     */

    public DoubleProperty getPreçoUnitarioProp() {
        return precoUnitario;
    }

    /**
     * @param preçoUnitario the preçoUnitario to set
     */
    public void setPreçoUnitario(DoubleProperty preçoUnitario) {
        this.precoUnitario = preçoUnitario;
    }



    public Integer getId() {
        return this.id; 
    }

    /**
     * @return the quantidade
     */
    public DoubleProperty getQuantidade() {
        return quantidade;
    }

    /**
     * @param quantidade the quantidade to set
     */
    public void setQuantidade(DoubleProperty quantidade) {
        this.quantidade = quantidade;
    }
    public void setQuantid(Double quantidade){
        this.quantidade.set(quantidade);
    }

    /**
     * @return the produtoData
     */

    
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DTO;

import java.io.Serializable;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;


/**
 *
 * @author igor
 */

public class Estoque implements AbstractDto<Integer>,Serializable {


    private Integer id; 
    

    private Produto produto;
    

    private DoubleProperty quantidade; 

    public Estoque(Double quantidade){
        this.quantidade = new SimpleDoubleProperty(quantidade);
    }
    
    public Integer getId() {
        return this.id; 
    }

    /**
     * @param id the id to set
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return the produto
     */
    public Produto getProduto() {
        return produto;
    }

    /**
     * @param produto the produto to set
     */
    public void setProduto(Produto produto) {
        this.produto = produto;
    }


    public double getQuantidade(){
       return this.quantidade.get();
    }
    /**
     * @return the quantidade
     */
    public DoubleProperty getQuant() {
        return quantidade;
    }

    /**
     * @param quantidade the quantidade to set
     */
    public void setQuantidade(DoubleProperty quantidade) throws Exception {
           this.quantidade = quantidade;  

    }
    public void setQuantidade(Double quantidade) throws Exception{
           this.quantidade.set(quantidade);  


    }
    }
    
   

package Controller;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */



import DTO.Produto;
import java.io.IOException;
import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;

/**
 *
 * @author igor
 */
public class Main extends Application {

    Main main;
    
   private ObservableList<Produto> produtoData = FXCollections.observableArrayList();
    
   private ObservableList<Produto> estoqueData = FXCollections.observableArrayList();
    
   private ObservableList<Produto> vendaData = FXCollections.observableArrayList();
    
   private final String title = "Trabalho Final";

    private Stage primaryStage;
    private BorderPane mainView;
    
     public static void main(String[] args) {
        try {
            launch(args);
        }catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        this.primaryStage = primaryStage;
        primaryStage.setTitle(title);

        // initializes primary layout
        initMainView();

        handleLogin("admin", "admin");
        //setScene(loadLoginView());
        
    }
    
        public void setScene(Parent root) {
        if (primaryStage.getScene() == null) {
            Scene scene = new Scene(root);
            primaryStage.setScene(scene);
        } else {
            primaryStage.getScene().setRoot(root);
        }

        //primaryStage.getIcons().add(new Image(getClass().getResourceAsStream("res/images/icon.png")));
        //primaryStage.sizeToScene();
        primaryStage.show();
    }

    private void initMainView() throws IOException {
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("/view/Menu.fxml"));
        
        mainView = loader.load();

        ((MenuController) loader.getController()).setMain(this);
    }

    private Parent loadLoginView() throws IOException {
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("./view/Login.fxml"));

        Parent root = loader.load();

        ((LoginController) loader.getController()).setMain(this);

        return root;
    }
    public Parent loadProduto() throws IOException{
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("/view/Produto.fxml"));

        Parent root = loader.load();

        ((ProdutoController) loader.getController()).setMain(this);

        return root;
    }
    
        public void loadProdutoNew() throws IOException {
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("view/ProdutoNew.fxml"));

        Parent root = loader.load();

        ((ProdutoEditController) loader.getController()).setMain(this);

            setScene(root);
        
    }
        
   
      /*  public boolean handleLogin(String text, String passwordFieldText) {
        if (text.equals("admin") && passwordFieldText.equals("admin")) {
            try {
                mainView.setCenter(loadMain());
                setScene(mainView);
                return true;
            } catch (IOException e) {
                e.printStackTrace();
                return false;
            }
        } else {
            return false;
        }
    }
*/
    public boolean handleLogin(String text, String passwordFieldText) {
        if (text.equals("admin") && passwordFieldText.equals("admin")) {
            try {
                mainView.setCenter(loadProduto());
                setScene(mainView);
               //initMainView();
                return true;
            } catch (IOException e) {
                e.printStackTrace();
                return false;
            }
        } else {
            return false;
        }
    }
    
    public void voltaMain() throws IOException{
        initMainView();
    }
    
    public ObservableList<Produto> getProdutoData() {
        return produtoData;
    }

    /**
     * @param produtoData the produtoData to set
     */
    public void setProdutoData(ObservableList<Produto> produtoData) {
        this.produtoData = produtoData;
    }

    /**
     * @return the estoqueData
     */
    public ObservableList<Produto> getEstoqueData() {
        return estoqueData;
    }

    /**
     * @param estoqueData the estoqueData to set
     */
    public void setEstoqueData(ObservableList<Produto> estoqueData) {
        this.estoqueData = estoqueData;
    }

    /**
     * @return the vendaData
     */
    public ObservableList<Produto> getVendaData() {
        return vendaData;
    }

    /**
     * @param vendaData the vendaData to set
     */
    public void setVendaData(ObservableList<Produto> vendaData) {
        this.vendaData = vendaData;
    }
    
}
   

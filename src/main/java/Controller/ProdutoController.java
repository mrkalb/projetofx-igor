/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import DTO.Estoque;
import DTO.Produto;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;

/**
 * FXML Controller class
 *
 * @author igor
 */
public class ProdutoController implements Initializable {

    Main main;
    
    @FXML
    private TableView<Produto> produto; 
    
    @FXML
    private TableColumn<Produto, String> codigo; 
    
    @FXML
    private TableColumn<Produto, String> descricao;
    
    @FXML
    private TableColumn<Produto, Number> preco; 
    @FXML
    private TableColumn<Produto, Number> estInicial; 
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        descricao.setCellValueFactory(elt -> elt.getValue().getDescricaoProp());
        preco.setCellValueFactory(elt -> elt.getValue().getPreçoUnitarioProp());
        estInicial.setCellValueFactory(elt -> elt.getValue().getQuantidade());
        
    }
    
    @FXML
    public void handlerNew() throws IOException{        
       Produto p = new Produto("",0.0,0.0);
       Estoque e = new Estoque(0.0);
       
     boolean temp =  ProdutoEditController.display(p,e);
     
     if(temp){
         main.getProdutoData().add(p);
         
     }
    }
    
    /**
     * Initializes the controller class.
     */

    public void setMain(Main main) {
        this.main = main;
        produto.setItems(main.getProdutoData());
    }
    
    
}
